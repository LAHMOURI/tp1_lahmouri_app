package com.example.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import com.example.tp1.data.Country;
public class DetailFragment extends Fragment {
    Country[] country=Country.countries;
    TextView population;
    TextView pays;
    TextView superficie;
    TextView monnaie;
    TextView capital;
    ImageView image;
    TextView Lalangue;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        image =view.findViewById(R.id.item_image1);
        pays =view.findViewById(R.id.item_country1);
        capital =view.findViewById(R.id.item_capital2);
        Lalangue =view.findViewById(R.id.item_capital4);
        monnaie =view.findViewById(R.id.item_monnaie);
        population =view.findViewById(R.id.item_population);
        superficie =view.findViewById(R.id.item_superficie);

        if(getArguments() != null) {
            DetailFragmentArgs dfa = DetailFragmentArgs.fromBundle(getArguments());
            int localis =dfa.getCountryId();
            Country paysA =country[localis];
            String uri= paysA.getImgUri();
            Context c=view.getContext();
            image.setImageResource(c.getResources().getIdentifier(uri,null,c.getPackageName()));
            pays.setText(paysA.getName());
            capital.setText(paysA.getCapital());
            Lalangue.setText(paysA.getLanguage());
            monnaie.setText(paysA.getCurrency());
            population.setText(String.valueOf(paysA.getPopulation()));
            superficie.setText(String.valueOf(paysA.getArea())+" km2");

        }





        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}