package com.example.tp1.data;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import com.example.tp1.ListFragmentDirections;
import com.example.tp1.R;
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    Country[] countrie =Country.countries;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView capitale;
        View vue;
        ImageView image;
        TextView country;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.item_image);
            country = itemView.findViewById(R.id.item_country);
            capitale = itemView.findViewById(R.id.item_capital);
            this.vue =itemView;
            int locali =getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int locali =getAdapterPosition();
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action=ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setCountryId(locali);
                    Navigation.findNavController(v).navigate(action);
                }
            });
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,parent,false);
        ViewHolder viewHolder=new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Country country= countrie[position];
        String uri=country.getImgUri();
        Context c=holder.vue.getContext();
        holder.image.setImageResource(c.getResources().getIdentifier(uri,null,c.getPackageName()));
        holder.country.setText(country.getName());
        holder.capitale.setText(country.getCapital());
    }

    @Override
    public int getItemCount() {
        return countrie.length;
    }


}
